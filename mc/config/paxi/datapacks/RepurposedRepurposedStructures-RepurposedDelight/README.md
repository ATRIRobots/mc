## Repurposed Repurposed Structures - excessive amounts of Minecraft structure datapacks!

![BANNER](http://i.creativecommons.org/p/zero/1.0/88x31.png)

A collection of [Repurposed Structures](https://github.com/TelepathicGrunt/RepurposedStructures) mod compatibility datapacks!
Feel free to use/edit them.
See their individual readmes for more info.

All packs are compatible with each other, unless otherwise stated, in which case there will usually be a compatibilty addon.

## Credits:

- TelepathicGrunt for Repurposed Structures
- Modding Legacy for Structure Gel, which was really helpful in creating/editing these structures
