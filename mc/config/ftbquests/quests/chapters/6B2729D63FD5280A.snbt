{
	id: "6B2729D63FD5280A"
	group: "73C5F0DEDDD06A0B"
	order_index: 3
	filename: "6B2729D63FD5280A"
	title: "Alex的动物图鉴"
	icon: "alexsmobs:animal_dictionary"
	default_quest_shape: "square"
	default_hide_dependency_lines: false
	quests: [
		{
			title: "白头海雕"
			x: 2.5d
			y: -5.0d
			description: [
				"白头海雕是在树木繁茂的山地生物群落中发现的大型中立猛禽。他们是附近湖泊的常客，在那里他们会尝试潜水并将鲑鱼从水中捞出。他们也喜欢以同样的方式攻击兔子。当攻击更大的生物时，白头海雕更喜欢盘旋，用爪子猛砍，重复如此。"
				""
				"由于他们对鱼的热爱，白头海雕可以用鱼油引诱和驯服。像许多其他野兽一样，驯服的白头海雕可以被命令留下、跟随和徘徊，但它们也可以成为猎鹰。一开始，需要制作猎鹰手套。无论是主手还是副手，这款手套都可以抓起白头海雕并将它们握在手中。要从手中发射鹰，只需一拳。如果在盯着一个生物发射时，猎鹰会飞向这个生物，攻击，如果它幸存下来，就会回到手上。如果没有选择怪物，猎鹰将从手套上跳下来。"
				""
				"为了提高猎鹰能力，可以制作一个猎鹰兜帽。兜帽可以戴在猎鹰身上，带兜帽的鹰在从手套上发射时会听从主人的直接命令。这意味着鹰可以由它的主人有效地引导。如果鹰飞离它的主人超过150个方块，它会试图返回。潜行可以停止控制猎鹰。兜帽可以用剪刀从鹰上取下。"
				""
				"白头海雕在狩猎时经常被猎物伤害。驯服的雕可以用任何鱼治愈。可以用腐烂的肉来繁殖。"
			]
			id: "594498C131B85266"
			tasks: [{
				id: "47CBC300B6803429"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/tame_bald_eagle"
				criterion: ""
			}]
		}
		{
			title: "大鳄龟"
			x: 2.5d
			y: 0.0d
			description: [
				"大鳄龟是一种体型巨大的半水生爬行动物，生活在整个世界的沼泽中。"
				"与其被动的海龟表亲不同，大鳄龟是一种凶猛的野兽，可以用锋利的喙状嘴攻击任何靠近它的东西。"
				"对于乌龟来说，咬合和移动速度惊人，遇到这些生物时，最好别管这些生物。 大鳄龟很少移动，在水下几天很常见，它们的龟壳上会生长苔藓。"
				""
				"如果长满苔藓，它们可以被剪掉，有机会掉落尖刺盾牌。 这些可用于升级海龟壳头盔。尖刺龟壳头盔内置了击退抗性，允许玩家在水下呼吸额外 15 秒。它也偶尔可以击退近战攻击。 鳄鱼鳄龟可以用生鳕鱼繁殖。"
			]
			id: "2FB0D01C17A241DA"
			tasks: [{
				id: "410D722202D61C76"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/alligator_snapping_turtle"
				criterion: ""
			}]
		}
		{
			title: "水滴鱼"
			icon: "alexsmobs:blobfish"
			x: 10.0d
			y: -2.5d
			description: ["水滴鱼是一种可以在深海被发现且身体呈凝胶状的鱼。虽然水滴鱼原本是灰色扁长形的，但如果它离开了低于十个方块水压的环境后，则它会变成一滩粉红色的烂泥状。当水滴鱼被杀死后，可以用四条水滴鱼和一个空瓶来合成鱼油。当鱼油浸没时，会有限度的漂浮在水中或雨中。它们和其它鱼类一样也可以被装着桶里。用一个粘液球右击水滴鱼后，它就可以在陆地上进行生存。"]
			id: "19B9C4D3752E283B"
			tasks: [{
				id: "4A077C248E11B21C"
				type: "item"
				item: "alexsmobs:fish_oil"
			}]
		}
		{
			title: "抹香鲸"
			x: 2.5d
			y: -2.5d
			description: [
				"抹香鲸是一种体型巨大的中性哺乳动物，生活在不冷不热的深海中，在那里可以看到它捕猎它最喜欢的猎物——鱿鱼。"
				"它们发出声音通过回声定位猎物，当它回声击中鱿鱼时，它会反弹回鲸鱼，以便它们确认其位置。随后鲸鱼持续加速并张开满是锋利牙齿的嘴猛击猎物。有时，鲸鱼可能会在此过程中掉下其中一颗长牙。"
				"这些鲸鱼在移动时能够破冰，但如果冲锋，它们可以打破船只和其他种类的木头。全白鲸非常罕见，它们比其他鲸鱼强壮得多。"
				""
				"抹香鲸可能看起来像一个巨人，但它仍受到一些自然力量的威胁。虎鲸喜欢吃年幼的抹香鲸，它们的母亲会极力保护它们免受伤害。在强大的雷暴期间，抹香鲸可能被冲上海滩。在这种无助的时候，如果他们被推回水中而没有受到伤害，可能会决定报答他们的救世主。"
				"报答的形式是给予玩家龙涎香龙涎香是鲸鱼咳出的胃的一种有价值的副产品。龙涎香可以交易给渔民村民换取大量绿宝石，或者可以用来制作回声定位器。它也可以用作强大的燃料，可以熔炼64种物品。"
				"回声定位器可以用来显示附近的洞穴。一旦发现洞穴，它就会向洞穴方向释放声波。由于此工具的使用数量有限，因此应谨慎使用。如果用4颗末影珍珠制作，你可以制作一个内定位器，当靠近据点时，它可以显示末端入口的位置。"
			]
			id: "0C5ACC52C7EE1E29"
			tasks: [{
				id: "05984E69201DBD70"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/save_cachalot_whale"
				criterion: ""
			}]
		}
		{
			title: "卷尾猴"
			x: 20.5d
			y: -7.5d
			description: [
				"卷尾猴是原产于丛林的小型灵长类动物。 像其他灵长类动物一样，它们喜欢香蕉，可以喂它们来驯服它们。驯服的卷尾猴比其他驯服的生物（如狼）更复杂，能够跟随、停留和徘徊，并且能够使用远程和近战攻击。它们也可以骑在主人的肩膀上，它们也可以从肩膀上攻击，向目标投掷石块。"
				""
				"卷尾猴可以喂食各种其他物品。 给它们喂鸡蛋可以治愈它们，还可以用蛆来繁殖它们，蛆是它们最喜欢的食物之一。偶尔，他们会在吃香蕉时掉下香蕉皮。 这些香蕉皮可用于制作一种独特的菜肴，被称为“猴汤”，厨师将其描述为“Uma Delicia”。"
			]
			id: "4A760B577E8528CC"
			tasks: [{
				id: "3E72FCD0EDA93AA7"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/tame_capuchin"
				criterion: ""
			}]
		}
		{
			x: 4.0d
			y: 0.0d
			description: [
				"笑鳄鱼是一种巨大的爬行动物，很少在整个世界的河流和沼泽中产卵。 这头野兽冲向它的猎物，抓住然后把它们拉到水下，在那里它继续进行毁灭性的死亡翻卷。 它对人和野兽都怀有敌意。"
				""
				"鳄鱼经常在白天上岸晒太阳。它会在这时候产卵。 鳄鱼蛋可以像鸡蛋一样扔，可能会生出鳄鱼宝宝。 年轻的怪物会印在最近的人身上，并将保卫它的诞生区域免受怪物的侵害。与它互动可以让它保持静止和晒太阳。"
				""
				"当生物成年或死亡时会掉落鳄鱼鳞甲。这些鳞甲可用于制作保护增加穿戴者游泳速度的胸甲。 它们也可用于制作本书的复刻。"
			]
			id: "2A81A0B0340C2D39"
			tasks: [{
				id: "7706BA89F6768AB5"
				type: "advancement"
				title: "鳄鱼"
				advancement: "alexsmobs:alexsmobs/crocodile"
				criterion: ""
			}]
		}
		{
			title: "乌鸦"
			x: 8.5d
			y: -5.0d
			description: [
				"乌鸦是在森林和平原发现的吵闹的鸟类。这些生物被认为是害虫，因为它们经常出现在田地周围并破坏作物生长。一个简单的雕刻南瓜就足以吓跑一小块田地里的任何乌鸦。每当靠近时，这些鸟通常会分散并飞上天空。乌鸦会捡起它们可以放进嘴里的任何食物并吞食它。"
				""
				"尽管乌鸦外表阴沉，但实际上可以通过向它们扔一些南瓜子来驯服它们。驯服的乌鸦是非常有用的生物，具有多种不同的用途。他们可以坐下、徘徊、跟随或收集物品。任何坐在干草块上的驯服的乌鸦都会慢慢恢复健康。稍后，当它们准备收集物品时，它们也会绕着这个干草块飞来飞去。当乌鸦在设置为收集物品时捡起一个物品，它会寻找一个容器，如箱子或熔炉，上面有一个与所持有的物品相匹配的物品展示框。"
				""
				"如果物品展示框中的物品与乌鸦嘴中的物品相同，它将从那一侧放入容器中。当准备跟随时，乌鸦会在它们的主人周围飞来飞去，并在一段时间后尝试坐在它们的肩膀上。乌鸦可以通过潜行从肩膀上移除。当乌鸦保护它们的主人时，它们会啄食攻击者，造成不会让怪物变得具有攻击性的低伤害。然而，乌鸦会对亡灵造成额外伤害。"
			]
			id: "7A0BD99BB1E192EC"
			tasks: [{
				id: "68DF0CEB2E2388DF"
				type: "kill"
				entity: "alexsmobs:crow"
				value: 1L
			}]
		}
		{
			title: "大象"
			x: 2.5d
			y: -7.5d
			description: [
				"大象是巨大的陆地哺乳动物，可以成群结队地在稀树草原上漫游。在大多数象群中，有三种大象：小象、长牙象和非长牙象。群落中通常有一两只长牙大象，明显比其他大象强壮。他们有一种特殊的、毁灭性的冲撞攻击。大象会保护它们的群落，并且大多数依靠这些长牙象提供保护。除了冲锋之外，大象还可以通过多种方式进行攻击，包括使用它们巨大的躯干来抛掷敌人，或者抬起并踩踏对手。"
				""
				"经常可以看到这些哺乳动物站起来从树上撕下叶子，为了寻找一种称为金合欢花的稀有食物。这些花偶尔从金合欢树上掉落，可以喂给大象来驯服它们。野生的长牙象无法驯服，但小象和非长牙象可以。长牙象只能通过驯服长牙象幼崽来获得。"
				""
				"驯服的大象可以通过与它们互动来骑乘。如果在大象身上使用箱子，它可以在背部获得相当大的库存，可以通过潜行访问。任何地毯也可以用来装饰大象。箱子和地毯都可以使用剪刀移除。驯服的长牙大象可以通过在骑乘时喂食小麦来进行冲锋。这种冲锋攻击会加速大象的速度并对范围内的任何生物造成大量伤害大象在冲锋后需要休息一段时间。"
				""
				"大象一旦被驯服，就可以用金合欢花繁殖。有时，温暖生物群落中的流浪商人会出现在商人大象的头顶上。这些大象是有箱子的，并且有一些可以获取的商人旅行的战利品。"
			]
			id: "76CCE0B2C18295B9"
			tasks: [{
				id: "622648EADADF5A21"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/tame_elephant"
				criterion: ""
			}]
		}
		{
			title: "鸸鹋"
			x: 10.0d
			y: -5.0d
			description: [
				"鸸鹋是一种大型的不会飞的鸟，在主世界的荒地和热带稀树草原上发现。 鸸鹋虽然外表略显滑稽，但也不是小鸟。传说这些生物曾经在与技术先进的国家的一场大战中获胜，仅仅是因为它们具有不可思议的躲避弹丸的能力，数量众多且速度快。受到攻击时，鸸鹋最初会惊慌失措并逃跑，但羊群中的一些鸸鹋会返回攻击。 它们用大爪子撕咬猎物。 鸸鹋不喜欢弓，因此它们有时会攻击骷髅。"
				""
				""
				"它们可以用小麦繁殖，通过繁殖可能会出现一些颜色图案。 鸸鹋偶尔会产下一个蓝色的大蛋，煮熟后可制成美味佳肴。 死亡时，他们可能会掉落一些羽毛，可以用来制作一套特殊的护腿，可以让穿着者躲避一些射弹，就像鸸鹋一样。 羽毛可用于制作横幅图案。"
			]
			id: "70DBD2450A950CA0"
			tasks: [{
				id: "6F21494774126F66"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/emu"
				criterion: ""
			}]
		}
		{
			title: "瞪羚"
			x: 14.5d
			y: -7.5d
			description: [
				"瞪羚是主世界热带稀树草原特有的一种小型哺乳动物。 瞪羚是一种精致的生物，非常胆小，可以观察到他们在群落中觅食。 当一只瞪羚被攻击时，该组的所有成员都会试图逃离危险，从任何感知到的威胁中跑得非常快。"
				""
				"瞪羚在被杀死时偶尔会掉落它们稀有的角。 这些角能与速度II药水 酿造为罕见的速度III药水。 他们也会掉羊肉。"
				"它们可以用金合欢花繁殖。"
			]
			id: "7C01F11EE012EE4E"
			tasks: [{
				id: "39A8B34E46EF3028"
				type: "kill"
				entity: "alexsmobs:gazelle"
				value: 1L
			}]
		}
		{
			title: "大猩猩"
			x: 7.0d
			y: -7.5d
			description: [
				"大猩猩是一种在丛林中成群结队地发现的大型猿类。 大猩猩是丛林的和平保护者，但这不代表他们毫无防备。 如果被激怒，大猩猩会造成巨大伤害。 每组大猩猩都有一个首领，黑猩猩，指挥他的部队穿越丛林。 它们的强度是其他大猩猩的两到三倍，而且经常可以看到他在焦虑时捶胸。 大猩猩宝宝在觅食时经常出现在它们父母的背上。"
				""
				"如果给予香蕉，大猩猩会变得可信和友好。 香蕉可以通过破坏丛林树叶获得。 如果给出足够的香蕉，大猩猩可以被命令坐下或徘徊，并且会保护他们的新朋友。"
			]
			id: "27E63ED670E5A48A"
			tasks: [{
				id: "27F8ABAC7F5D00E2"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/tame_gorilla"
				criterion: ""
			}]
		}
		{
			title: "灰熊"
			x: 4.0d
			y: -7.5d
			description: [
				"灰熊是一种在森林生物群落中发现的大型杂食哺乳动物。 它通常与一两只熊崽一起被发现，它们跟随他们的父母。 任何没有准备的旅行者或猎人都应该与灰熊保持距离，就好像他们是远距离中立，但接近时变成敌对。 众所周知，灰熊会袭击任何有蜂蜜的蜂箱，撕开它并享用里面的蜜脾。 如果灰熊最近吃饱蜂蜜了，它可以安全地靠近。 据信，在灰熊吃饱蜂蜜后喂它三文鱼会获得野兽的信任，尽管很少有人能在与这些熊的近距离接触中幸存下来。 （来自声称它们可以成为战争坐骑的人。）"
				""
				""
				"有时，灰熊可能会脱落一些毛发。 这些头发可用于酿造击退抗性药水，当与力量药水结合。"
			]
			id: "4D7900A091631CA2"
			tasks: [{
				id: "21FBB7D35D383987"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/grizzly_bear"
				criterion: ""
			}]
		}
		{
			title: "锤头鲨"
			x: 5.5d
			y: -2.5d
			description: ["锤头鲨是一种大型的食肉性鱼类，生活在温暖的水域，以其捕食性而闻名。锤头怪会攻击任何健康低于一半的生物，是一种拾荒兽。 他们也喜欢热带鱼和鱿鱼。 寻找食物时，锤头鲨鱼事先盘旋猎物，以寻找理想的攻击角度。 有时，在攻击猎物时，锤头鲨可能会松动它的许多牙齿中的一颗。 这些牙齿可以用来制造可以更好地在水下行进并对水生生物造成额外伤害的箭。"]
			id: "050982B79803A50C"
			tasks: [{
				id: "3000E8B44873A570"
				type: "kill"
				entity: "alexsmobs:hammerhead_shark"
				value: 1L
			}]
		}
		{
			title: "蜂鸟"
			x: 5.5d
			y: -5.0d
			description: ["蜂鸟是已知存在的最小的鸟类，但这并不能阻止它们快速飞行。 这些小鸟可以被发现于丛林、繁花森林和向日葵生物群系。鲜花 - 可以使这些小动物平静的东西。蜂鸟是庄稼和花卉的强力传粉者，甚至比蜜蜂更擅长传粉。 它们可以用鲜花繁殖。"]
			id: "0EEC85B170E565E0"
			tasks: [{
				id: "51F4DFB74F93ADAE"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/breed_hummingbird"
				criterion: ""
			}]
		}
		{
			title: "袋鼠"
			x: 5.5d
			y: -7.5d
			description: [
				"袋鼠是大型的中立有袋动物，原产于热带草原和荒地。这些生物有独特的运动方式：它们只能通过跳跃来移动。"
				""
				"有时可以看到他们的小袋里有一只幼袋鼠。尽管它们的史前外观，袋鼠是严格的食草动物，喜欢以草为食。然而，他们不是好欺负的。如果受到攻击，袋鼠可以双脚拳打脚踢，造成毁灭性的攻击。"
				""
				"被杀死时，他们会掉落一些肉，这些肉可以煮熟并制成美味的袋鼠汉堡。他们还可以掉落一些皮革，这些皮革可以制作成皮革或独特的旗帜图案。"
				""
				"袋鼠可以用十到十五根胡萝卜来驯服。驯服后，您可以通过与它们互动来命令它们留下、跟随或徘徊，并通过潜行访问袋鼠袋子的库存。如果袋子里放有近战武器，袋鼠会在每次攻击时使用它。"
				""
				"成年袋鼠还可以装备任何放在袋子里的头盔或胸甲，并且可以在受伤时吃掉放在袋子里的任何非肉类食物来恢复健康。袋鼠可以用枯死的灌木或一些草繁殖。"
			]
			id: "32A3A5B3A3956B69"
			tasks: [{
				id: "75FA66DBC18F8E4A"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/kangaroo"
				criterion: ""
			}]
		}
		{
			title: "科莫多巨蜥"
			x: 11.5d
			y: -7.5d
			description: [
				"科莫多巨蜥是世界上最大的蜥蜴，经常在偏远的荒地生物群落中发现。 这些大蜥蜴将人和野兽视为食物，几乎可以攻击任何东西。它们也是同类相食的，会攻击任何其他虚弱的科莫多巨蜥，尤其是小科莫多巨蜥。 这些蜥蜴的咬伤非常具有毒性，它们用它来缩小猎物的体型。"
				""
				"科莫多龙喜欢腐肉，会从玩家身上吃掉整堆腐肉。 如果它们被喂食多堆腐肉，它们就可以被玩家驯服。驯服的科莫多和野生的科莫多一样野蛮，但它们可以被用作速度惊人的强大坐骑。"
				""
			]
			id: "6B32911C18B643D3"
			tasks: [{
				id: "24F8C97999D4B840"
				type: "kill"
				entity: "alexsmobs:komodo_dragon"
				value: 1L
			}]
		}
		{
			title: "龙虾"
			icon: "alexsmobs:lobster_tail"
			x: 8.5d
			y: -2.5d
			description: ["龙虾是小型水生甲壳类动物，可以在海滩生物群落的水下找到。 如果受到攻击，龙虾会夹住附近任何有攻击性的怪物，但不会追逐他们。 虽然最常见的是红色，但龙虾也可以有其他更稀有的颜色。如果被杀死，龙虾偶尔会掉下可以烹饪的美味龙虾尾。 它们也可以像任何种类的鱼一样被放入桶中。"]
			id: "22CA803AEDA8FDFD"
			tasks: [{
				id: "178B4164EBA185D4"
				type: "kill"
				icon: "alexsmobs:lobster_tail"
				entity: "alexsmobs:lobster"
				value: 1L
			}]
		}
		{
			title: "驼鹿"
			x: 13.0d
			y: -7.5d
			description: ["驼鹿是巨大的食草哺乳动物，栖息在积雪的苔原和积雪的针叶林中。 然而，它们不是胆小的食草动物。如果受到攻击，它们可以使用巨大的鹿角造成危险的伤害。 经常可以发现驼鹿为了占主导地位而与其他驼鹿角逐角。每七到十天，驼鹿就有机会掉落一只罕见的巨大鹿角。 据说其中两个鹿角可以用来制作头饰，赋予佩戴者特殊的击退能力。驼鹿需要三到五天的时间才能重新长出它们的角。 驼鹿是好父母，自然会保护他们的孩子免受包括狼在内的任何掠食者的伤害。驼鹿可以用多个蒲公英繁殖，这是它们最喜欢的食物。"]
			id: "6C28CFA5CE33919A"
			tasks: [{
				id: "71A00442FAE4C1A8"
				type: "item"
				item: "alexsmobs:moose_antler"
			}]
		}
		{
			title: "虎鲸"
			x: 4.0d
			y: -2.5d
			description: [
				"虎鲸是一种大型的、类似海豚的鲸目动物，生活在寒冷的海洋中。经常可以看到它在上升和下降并对当地的北极熊种群发起攻击。不像他们的海豚表亲，这些鲸鱼并不友好——伤害其中一头可能会让整个集群攻击你。"
				""
				"与一群虎鲸一起游泳可以获得逆戟之力效果。这个效果可以提高攻击速度。虎鲸经常用自己的力量对付溺尸和守卫者。"
			]
			id: "2B10B614DBDF56A9"
			tasks: [{
				id: "67CE1E0C13C45D56"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/orcas_might"
				criterion: ""
			}]
		}
		{
			title: "鸭嘴兽"
			x: 17.5d
			y: -7.5d
			description: [
				"鸭嘴兽是一种看起来像其他几种动物的组合，就其本身而言却是独一无二的。 偶尔能在主世界的河流生物群系中遇到它。这种奇怪的哺乳动物脚上长着有毒的刺，用来对任何攻击者。"
				""
				"鸭嘴兽的嗅觉、听觉和视力较弱，导致它依靠电力来捕猎和寻路。 因此，鸭嘴兽会被红石吸引。 如果喂食红石，鸭嘴兽会恢复其电灵敏度，并会在附近的粘土块中寻找一些蛆虫。在寻找食物时，鸭嘴兽会挖出几个粘土球和蛆虫，然后收集它们。 如果喂食整个红石块，挖出的物品数量会大大增加。像其他小型水生生物一样，鸭嘴兽可以用一桶水运输。 它可以用它最喜欢的食物龙虾尾饲养。"
			]
			id: "5F70363E3CCC02B1"
			tasks: [{
				id: "72F03AFBF88FDFFB"
				type: "kill"
				entity: "alexsmobs:platypus"
				value: 1L
			}]
		}
		{
			title: "浣熊"
			x: 19.0d
			y: -7.5d
			description: [
				"浣熊是平原与森林原生的小型哺乳类动物。尽管它是中立的，当它看到人手里拿着食物时，还是会向人们乞求。因为它们亮晶晶的双眼，它们经常能在晚上被认出来."
				""
				"虽然喂养这些可爱的动物可能很诱人，但通常没有什么好处。 浣熊是一个众所周知的小偷，经常从箱子和桶里偷食物，尽管它的外表很可爱，但可能非常具有攻击性。浣熊有趣的行为之一是它的“清洗”行为。 当食物靠近水时，浣熊会在水下清洗食物，然后吃掉它。"
				""
				"只有当把鸡蛋放在靠近水的地方，浣熊才能被驯服，在那里它们会根据自己的需要清洗鸡蛋。 驯服的浣熊可以坐下、跟随和游荡。 它们可以用面包繁殖。当被杀死时，浣熊有机会掉下它们的尾巴。 这些尾巴可用于制作拓荒者的帽子，在潜行时提供速度加成。"
			]
			id: "0397441DDE19834E"
			tasks: [{
				id: "0A090EEEFB667EF2"
				type: "item"
				item: "alexsmobs:raccoon_tail"
			}]
		}
		{
			title: "响尾蛇"
			x: 22.0d
			y: -7.5d
			description: [
				"响尾蛇是一种在沙漠和荒地中发现的中型爬行动物。 大多数时候，响尾蛇是被听到的，而不是被看到的，因为当潜在的捕食者在附近时，它会发出响亮的响尾警告声。  如果一个人足够接近响尾蛇而发出响尾声时，该生物会变得充满敌意并使用它的毒咬来保护自己。  响尾蛇是走鹃的天敌，是走鹃最常见的捕食者。"
				""
				"当响尾蛇被杀死，有几率掉落响尾。 这些响尾可以用瓶装科莫多龙唾液酿造来制作一种肮脏的毒性精华。 "
			]
			id: "18F4392B8FEA6FB5"
			tasks: [{
				id: "6576F33ACEB15CD1"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/rattlesnake"
				criterion: ""
			}]
		}
		{
			title: "走鹃"
			x: 11.5d
			y: -5.0d
			description: ["走鹃是一种中型鸟类，可以在沙漠和荒地中奔跑。 以它的大小来说，非常快速和敏捷。 走鹃经常与响尾蛇，其他沙漠居民，他们的天然猎物发生冲突。  据说这种鸟的羽毛可以用来制作靴子，使穿戴者在沙子上跑得飞快，因此受到高度追捧。"]
			id: "72EA6ACE7AD91F5A"
			tasks: [{
				id: "2CFBADB8EE67438F"
				type: "item"
				item: "alexsmobs:roadrunner_feather"
			}]
		}
		{
			title: "海鸥"
			x: 7.0d
			y: -5.0d
			description: [
				"海鸥是一种常见于海滩上的中型鸟类。 这些鸟因其能够窃取任何食物而引人注目，无论它们是手持还是在快捷栏中。他们还会捡起并吃掉地上的任何食物。 海鸥会每隔一到三分钟偷东西，所以虽然很烦人，但它们并非威胁。"
				""
				"然而，海鸥不仅仅是一种烦人的鸟。 如果你在副手持有一张藏宝图，他们可以将龙虾尾扔到地上。 吃掉它的海鸥会直接坐在埋藏的宝藏上方来奖励它的饲养者。一旦宝藏离地图上的“X”足够近，这将极大地帮助定位宝藏。 海鸥也可以用生鳕鱼养殖。"
			]
			id: "261D03EDF9564440"
			tasks: [{
				id: "7F0FA180BA3ED4F5"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/seagull_steal"
				criterion: ""
			}]
		}
		{
			title: "海豹"
			x: 7.0d
			y: -2.5d
			description: ["海豹大多是是水生哺乳动物，可以在主世界的海滩和冰山上找到。 这些看似懒惰的生物经常在它们能找到的任何沙子或冰上晒太阳和睡觉。当它们不在陆地上休息时，海豹是一个非常熟练的游泳者，能够在其他大多数水生物种周围游泳，除了它们的终极捕食者：虎鲸。 如果一个海报被攻击，整个群落都会想办法逃入水中，因为在水中它们更加敏捷。 在陆地上喂一只晒太阳的海豹三条鱼将引发人与海豹之间的原始“交易”：为了换取三条鱼，海豹会在海底搜寻物品并带回来。尽管此物品通常是海带或沙子等毫无价值的垃圾，但有时也可能是稀有得多的物品，例如鲨鱼牙齿、鹦鹉螺壳。 海豹可以用龙虾尾繁殖。"]
			id: "0C517A9D536786E3"
			tasks: [{
				id: "4BAD3ADB4F7ECC49"
				type: "kill"
				entity: "alexsmobs:seal"
				value: 1L
			}]
		}
		{
			title: "鲸头鹳"
			x: 13.0d
			y: -5.0d
			description: ["鲸头鹳是能在沼泽中发现的奇怪的鹳鸟。 这些看起来令人生畏的鸟实际上非常胆小，并且在受到攻击时会展翅高飞。人们经常看到它们摇晃着大而奇怪的喙。 当单独留下时，经常可以看到鞋鹳在钓鱼，捕捉他们最喜欢的食物。 很多时候，鞋鹳会提供对它们无用的副渔获物，但并非对每个人都如此。鲸头鹳可以喂鳄鱼蛋来增加对它们的捕获物的“诱惑”效果，喂食水滴鱼来增加它们捕获物的“运气”。 鞋鹳生性胆小，不能被人饲养。除了鱼，鞋鹳还会攻击小鳄鱼和海龟，这也是它们饮食的一部分。"]
			id: "1F7BF07E35B668D5"
			tasks: [{
				id: "12731F53CC561621"
				type: "kill"
				entity: "alexsmobs:shoebill"
				value: 1L
			}]
		}
		{
			title: "雪豹"
			x: 10.0d
			y: -7.5d
			description: [
				"雪豹是大型猫科动物，可以在下雪的山找到。 这些雪豹虽然是贪婪的猎人，但对人类并不具有攻击性。然而，他们会跟踪牲畜，尤其是他们最喜欢的猎物绵羊。 雪豹会弓起背部并潜入猎物身后，然后发动毁灭性的跳跃攻击。这些大型猫科动物也可以在近战中使用爪子进行攻击。 雪豹是非常熟练的捕食者，它们的杀戮通常会产生额外的掉落物，就好像这种生物具有很高的掠夺性。"
				""
				"尽管雪豹无法被驯服，但它们可以用驼鹿肋骨和肉进行饲养，这是一种它们梦寐以求的肉。这些大型猫科动物并不耳软心活，它们会保护自己和后代免受任何攻击。"
			]
			id: "2A86B564DD123C89"
			tasks: [{
				id: "3B37EF423460B47C"
				type: "kill"
				entity: "alexsmobs:snow_leopard"
				value: 1L
			}]
		}
		{
			title: "太阳鸟"
			x: 4.0d
			y: -5.0d
			description: [
				"太阳鸟是一种神鹤，在最高的山脉上空翱翔。 它们完全被动，太阳鸟会将它的祝福授予所有能接近它的玩家。当受到太阳鸟的祝福，人们会发现用鞘翅飞行更容易，并且可以平静地以加速的速度上下浮动，就像太阳鸟一样。 然而，如果太阳鸟被攻击，一种天罚会结成联盟，导致攻击者迅速倒地，而且往往是致命的。"
				""
				"作为神圣的生物，太阳鸟会使邪恶的不死生物在靠近时立即燃烧起来，就好像它们在强大的阳光下。 太阳鸟不会被任何类型的火焰、热量或熔岩伤害，因为它们天生就火焰的化身。"
			]
			id: "403CD03A5979466E"
			tasks: [{
				id: "5E31ADC4F5EEECB5"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/sunbird_blessing"
				criterion: ""
			}]
		}
		{
			title: "袋灌"
			x: 16.0d
			y: -7.5d
			description: ["袋獾是在温带森林生物群落中很罕见的小型小型有袋动物。 这些生物可以通过它们的黑白外套和奇怪的咆哮来识别。 经常可以看到他们成群结队地坐着或晒太阳。 当攻击他们最喜欢的兔子或鸡猎物时，附近的所有袋獾都会加入狩猎。 如果喂食它们最喜欢的腐肉食物，袋獾会发出一声嚎叫，可以在短时间内吓跑附近的所有怪物。 这些暴躁的小动物无法驯服，但可以用任何肉来饲养它们。"]
			id: "2CD9B223606A5B3F"
			tasks: [{
				id: "16F553E78C5B2F14"
				type: "kill"
				entity: "alexsmobs:tasmanian_devil"
				value: 1L
			}]
		}
		{
			title: "老虎"
			x: 8.5d
			y: -7.5d
			description: [
				"老虎是大型猫科动物，可以在竹林内部和周围找到。他们天生厌倦外来者，一旦接近就会发动攻击。老虎是个优秀的猎手，而且非常强壮，能从许多街区外一跃而下粉碎猎物，还能用爪子劈砍。他们也是隐身大师，因为当他们从远处跟踪猎物时，他们通常能够变得几乎隐形。他们也非常快。最重要的是，它们是如此可怕，以至于当老虎出现时，它们的猎物会在恐惧中僵住几秒钟。"
				""
				"尽管他们的力量和可怕的外表，这些生物远非怪物。通过主要喂它们鸡肉或猪排、它们最喜欢的食物或其他新鲜肉类，可以获得老虎的祝福，这种效果可以让人们接近老虎而不会受到伤害。 如果你受到攻击，此效果还会使老虎帮助您。然而，它只持续几分钟，这意味着人们必须继续喂养一只老虎以保持它的中立性。如果攻击老虎，该效果会丢失。 老虎父母非常具有防御性。尽管它们主要是食肉动物，但它们可以使用金合欢花繁殖。偶尔在繁殖老虎时，幼崽有几率成为稀有的小白虎。"
			]
			id: "25C1881E2F7E7067"
			tasks: [{
				id: "7493322198E38CE1"
				type: "advancement"
				advancement: "alexsmobs:alexsmobs/tigers_blessing"
				criterion: ""
			}]
		}
	]
}
